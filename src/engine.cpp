#include <algorithm>
#include <boost/filesystem.hpp>
#include <cfloat>
#include <cmath>

#include "constant.hpp"
#include "engine.hpp"

#ifdef WITH_PYTHON
#include <Python.h>
#endif

/**
 * @brief Initialize the engine
 *
 * @param parts The list of particles
 * @param box_size The box size
 * @param eps The gravitational softening
 * @param dt_max The maximal time step allowed
 * @param periodic Periodic boundary condition
 */
Engine::Engine(std::vector<class Particle> parts, float box_size, float eps, double dt_max, bool periodic) {
  if (periodic) {
    std::cerr << "WARNING: the periodic gravitational forces are not correctly taken into account" << std::endl;
  }

  /* Set the scalar variables */
  this->box_size = box_size;
  this->eps2 = eps * eps;
  this->dt = 0;
  this->dt_max = dt_max;
  this->opening_angle = 0.6;
  this->periodic = periodic;
  this->c_star = 0.05;
  this->r_acc = 0.001;
  
  this->time = 0;
  this->step = 0;
  this->policy = policy_grav | policy_hydro;

  /* Deal with snapshots */
  this->snapshot_number = 0;
  const char* path = "./snap";
  boost::filesystem::path dir(path);
  if(boost::filesystem::create_directory(dir)) {
    std::cerr<< "Directory Created" << std::endl;
  }
  

  /* Print all the supposition */
  std::cout << "Supposing:" << std::endl
	    << "\t Opening angle: " << this->opening_angle << std::endl
	    << "\t h_max: " << hydro_h_max << std::endl
	    << "\t G = " << G << std::endl
	    << "\t r_acc = " << this->r_acc << std::endl
	    << "\t cooling rate = " << high_cooling_rate
	    << " " << low_cooling_rate << std::endl
	    << "\t Njeans = " << n_jeans << std::endl << std::endl;

  /* Print message */
  std::cout << "Step" << "\t" << "Time" << "\t" << "Timestep" << "\t" << "hydro_parts" << std::endl;

  
  this->n_parts = parts.size();
  this->parts.resize(this->n_parts);

  /* Init parts */
  for(size_t i = 0; i < this->n_parts; i++) {
    this->parts[i] = parts[i];
    this->parts[i].id = i;
  }

  /* Init Cells */
  const float cell_size = box_size / engine_top_cell;
  for(int i = 0; i < engine_n_top_cells; i++) {
    int a = i;
    int b, c;
    get_cell_index(&a, &b, &c, engine_top_cell);

    float pos[3] = {
      a * cell_size,
      b * cell_size,
      c * cell_size,
    };
    top_cells[i] = Cell(cell_size, pos, &this->parts);
  }

  /* Compute the densities */
  this->computeDensity();

  /* Convert the energy */
  for(size_t i = 0; i < this->n_parts; i++) {
    if (this->parts[i].type == type_hydro) {
      this->parts[i].convertEntropy(this);
    }
  }

  /* Compute the time step */
  this->dt = this->getTimeStep();
  if (this->dt > this->dt_max) {
    this->dt = this->dt_max;
  }

}


/**
 * @brief Remove the particles that leave the box
 */
void Engine::removeParticleOutOfBox() {
  size_t i = 0;
  while(i < this->parts.size()) {

    /* Check if outside of the box */
    bool del = false;
    for(int k = 0; k < 3; k++) {
      if (this->parts[i].pos[k] < 0 ||
	  this->parts[i].pos[k] >= this->box_size) {
	del = true;
	break;
      }
    }

    /* Everything is fine */
    if (!del) {
      i++;
      continue;
    }

    /* Particle left the box -> delete */
    this->parts.erase(this->parts.begin() + i);
    this->n_parts -= 1;
  }
}

/**
 * @brief Tag the particles according to their top level cells
 */
void Engine::tagParticles() {
  /* Assign tag for each particle */
  for(size_t i = 0; i < this->n_parts; i++) {
    bool found = false;

    /* Look at each cell */
    for(int j = 0; j < engine_n_top_cells; j++) {
      if (this->top_cells[j].inCell(&this->parts[i])) {
	this->parts[i].sort_key = j;
	found = true;
	break;
      }
    }

    if (!found) {
      this->print();
      sink_error("Unable to find the correct cell %g %g %g",
		 this->parts[i].pos[0],
		 this->parts[i].pos[1],
		 this->parts[i].pos[2]);
    }
  }

}

/**
 * @brief Generate the whole cell hierarchy
 */
void Engine::splitCells() {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].splitCell();
  }

}

/**
 * @brief Sort the particles into the correct top level cell
 */
void Engine::sortCells() {
  /* Tag the particles */
  this->tagParticles();

  /* Sort the array */
  std::sort(this->parts.begin(), this->parts.end());

  /* assign arrays to cells */
  size_t i = 0;
  for(int c = 0; c < engine_n_top_cells; c++) {
    class Cell *cell = &this->top_cells[c];
    /* Count number particles */
    cell->start = i;
    while (i < this->n_parts && this->parts[i].sort_key <= c) {
      i++;
    }

    int n_parts = i - cell->start;
    cell->n_parts = n_parts;
  }

}

/**
 * @brief Print all the top level cells
 */
void Engine::print() {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].print();
  }
}

/**
 * @brief Compute the h_max of all the top level cells
 */
void Engine::computeHmax() {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].getHmax();
  }
}

/**
 * @brief Compute the cell hierarchy
 */
void Engine::makeCells() {
  /* Cleanup cells */
  this->clean();

  /* Sort the top cells */
  this->sortCells();

  /* Split the cells */
  this->splitCells();

  /* Compute the gravitational quantities */
  if (this->policy & policy_grav)
    this->computeCenterMass();

  /* Compute the hydro quantities */
  if (this->policy & policy_hydro)
    this->computeHmax();
}

/**
 * @brief Compute the cells center of mass
 */
void Engine::computeCenterMass() {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].computeCenterMass();
  }
}

/**
 * @brief Remove the cell hierarchy
 */
void Engine::clean() {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].clean();
  }
}

/**
 * @brief Compute the gravitational interaction
 */
void Engine::doGravity() {
  class Cell *cells = this->top_cells.data();
#pragma omp parallel
  {
#pragma omp single
    {
      for(int i = 0; i < engine_n_top_cells; i++) {
	class Cell *ci = &cells[i];
#pragma omp task depend(in: cells[i]) depend(out: cells[i])
	ci->gravityInteractCells(ci, this);
	for(int j = i+1; j < engine_n_top_cells; j++) {
	  class Cell *cj = &cells[j];

	  /* Do the interactions */
#pragma omp task depend(in: cells[i]) depend(out: cells[i])
	  ci->gravityInteractCells(cj, this);
#pragma omp task depend(in: cells[j]) depend(out: cells[j])
	  cj->gravityInteractCells(ci, this);
	}
      }
    }
  }
}

/**
 * @brief Update the particles
 *
 * @param type The type of update
 */
void Engine::updateParticles(int type) {
  for(int i = 0; i < engine_n_top_cells; i++) {
    this->top_cells[i].updateParticles(type, this);
  }
}

/**
 * @brief Compute the next time step
 *
 * @return the time step 
 */
float Engine::getTimeStep() {

  float dt = FLT_MAX;
  for(int i = 0; i < engine_n_top_cells; i++) {
    float tmp = this->top_cells[i].getTimeStep(this);
    if (tmp < dt)
      dt = tmp;
  }

  return dt;
}

/**
 * @brief Compute one hydro interaction
 *
 * @param type The type of interaction (e.g. force, density)
 */
void Engine::doHydro(int type) {
  class Cell *cells = this->top_cells.data();
#pragma omp parallel
  {
#pragma omp single
    {
      for(int i = 0; i < engine_n_top_cells; i++) {
	class Cell *ci = &cells[i];
#pragma omp task depend(in: cells[i]) depend(out: cells[i])
	ci->hydroInteractCells(ci, type, this);
	for(int j = i+1; j < engine_n_top_cells; j++) {
	  class Cell *cj = &cells[j];

	  /* Do the interactions */
#pragma omp task depend(in: cells[i]) depend(out: cells[i])
	  ci->hydroInteractCells(cj, type, this);
#pragma omp task depend(in: cells[j]) depend(out: cells[j])
	    cj->hydroInteractCells(ci, type, this);
	}
      }
    }
  }
}

/**
 * @brief Do a ghost computation
 */
void Engine::doGhost() {
  /* Reset the ghost counter */
  this->step_ghost = 0;

  class Cell *cells = this->top_cells.data();
  size_t todo = this->n_parts;
  bool reset = true;
  while(todo > 0 && this->step_ghost < max_smoothing_iter) {
    todo = 0;
#pragma omp parallel shared(todo)
    {
#pragma omp for
      for(int i = 0; i < engine_n_top_cells; i++) {
	class Cell *ci = &cells[i];

	/* Do we still have some particles to update */
	if (ci->ghost_todo == 0) {
	  continue;
	}

	/* Do the ghost */
	ci->doGhost(this, reset);

	/* Do we need a new step? */
	if (ci->ghost_todo == 0) {
	  continue;
	}

	/* Update the counter */
#pragma omp atomic
	todo += ci->ghost_todo;

	/* Do a new density computation */
	for(int j = 0; j < engine_n_top_cells; j++) {
	  class Cell *cj = &cells[j];
	  /* Do the interactions */
#pragma omp task depend(in: cells[i]) depend(out: cells[i])
	  ci->hydroInteractCells(cj, interaction_density | interaction_ghost, this);
	    
	}
      }
    }
    this->step_ghost += 1;
    reset = false;
  }
      
  if (this->step_ghost >= max_smoothing_iter)
    sink_error("Failed to converge for the smoothing length");
}

/**
 * @brief Compute the density
 */
void Engine::computeDensity() {
#ifdef WITH_PYTHON
  Py_BEGIN_ALLOW_THREADS;
#endif

  /* Create the cells */
  this->makeCells();

  /* Compute hydro density */
  this->doHydro(interaction_density);

  /* Do hydro ghost */
  this->doGhost();

#ifdef WITH_PYTHON
  Py_END_ALLOW_THREADS;
#endif
}

/**
 * @brief Compute the average acceleration
 */
void Engine::computeAcceleration() {
  float h_acc[3] = {0.};
  float acc[3] = {0.};

  float abs_h_acc[3] = {0.};
  float abs_acc[3] = {0.};
  
  for(size_t i = 0; i < this->n_parts; i++) {
    class Particle *p = &this->parts[i];
    for(int k = 0; k < 3; k++) {
      acc[k] += p->acc[k];
      abs_acc[k] += std::abs(p->acc[k]);

      if (p->type == type_hydro) {
	h_acc[k] += p->hydro.acc[k];
	abs_h_acc[k] += std::abs(p->hydro.acc[k]);
      }
    }
  }

  /* Normalize */
  for(int k = 0; k < 3; k++) {
    acc[k] /= abs_acc[k];
    h_acc[k] /= abs_h_acc[k];
  }
  

  std::cout << "Grav acc: " << acc[0]
	    << ", " << acc[1]
	    << ", " << acc[2] << std::endl;
  std::cout << "Hydro acc: " << h_acc[0]
	    << ", " << h_acc[1]
	    << ", " << h_acc[2] << std::endl;
}


/**
 * @brief Compute the total momentum
 */
void Engine::computeMomentum() {
  float p[3] = {0.};
  float p2 = 0.;
  
  for(size_t i = 0; i < this->n_parts; i++) {
    float mass = this->parts[i].mass;
    for(int k = 0; k < 3; k++) {
      p[k] += this->parts[i].vel[k] * mass;
      p2 += p[k] * p[k];
    }
  }

  std::cout << "Momentum " << std::sqrt(p2)
	    << ": " << p[0]
	    << ", " << p[1]
	    << ", " << p[2] << std::endl;
}

/**
 * @brief Evolve the system (main function)
 */
void Engine::doStep(float time) {
#ifdef WITH_PYTHON
  Py_BEGIN_ALLOW_THREADS;
#endif

  /* Check if only one step is required */
  if (time < 0)
    time = this->time + this->dt;

  /* compute steps until reaching required time */
  while(this->time < time) {
    std::cout << this->step << "\t" << this->time << "\t" << this->dt << "\t" << this->n_parts << std::endl;

    /* Do drift */
    this->updateParticles(update_acceleration | update_drift);

    /* Remove particles outside of the box */
    if (!this->periodic) {
      this->removeParticleOutOfBox();
    }

    /* Create the cells */
    this->makeCells();

    /* Compute gravity accelerations */
    if (this->policy & policy_grav)
      this->doGravity();

    /* Do the hydro */
    if (this->policy & policy_hydro) {
      /* Compute hydro density */
      this->doHydro(interaction_density);

      /* Do hydro ghost */
      this->doGhost();

      /* Compute hydro accelerations */
      this->doHydro(interaction_hydro_force);
    }

    /* Deal with the sink particles */
    if (this->policy & policy_stars) {

      /* Form sink particles */
      this->updateParticles(update_sink_formation);
    }

    /* Do second kick */
    this->updateParticles(update_half_kick | update_end_force |
			  update_reset_predict | update_cooling);

    /* Compute new time step */
    this->step += 1;
    this->time += this->dt;
    this->dt = this->getTimeStep();

    /* Stay below dt max */
    if (this->dt > this->dt_max)
      this->dt = this->dt_max;

    if (this->dt == 0.)
      sink_error("Null time step");
  
    const double new_time = this->time + this->dt;
    if (new_time > time) {
      this->dt = time - this->time;
    }

    /* Do first kick */
    this->updateParticles(update_half_kick);
  }

#ifdef WITH_PYTHON
  Py_END_ALLOW_THREADS;
#endif
}


/**
 * @brief Write a snapshot
 */
void Engine::writeSnapshot() {

  char filename[200];

  /* Open output file for gas */
  sprintf(filename, "./snap/snapshot_0_%04i.bin", this->snapshot_number);
  std::ofstream snap;
  snap.open(filename, std::ios::out | std::ios::binary);

  /* Write the particles */
  for(unsigned int i = 0; i < this->n_parts; i++) {
    if (this->parts[i].type == type_hydro) {
      /* Position */
      snap.write((const char *)&this->parts[i].pos[0], 3 * sizeof(float));
      /* Velocity */
      snap.write((const char *)&this->parts[i].vel[0], 3 * sizeof(float));
      /* Acceleration */
      snap.write((const char *)&this->parts[i].acc[0], 3 * sizeof(float));
      /* mass */
      snap.write((const char *)&this->parts[i].mass, sizeof(float));
      /* Id */
      snap.write((const char *)&this->parts[i].id, sizeof(int));
      /* Smoothing length */
      snap.write((const char *)&this->parts[i].hydro.h, sizeof(float));
      /* Density */
      snap.write((const char *)&this->parts[i].hydro.rho, sizeof(float));
      /* Energy */
      const float u = this->parts[i].getEnergy();
      snap.write((const char *)&u, sizeof(float));
    }
  }

  snap.close();

  /* Increase the counter */
  this->snapshot_number += 1;
}
