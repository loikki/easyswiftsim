#include <array>

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "engine.hpp"
#include "cell.hpp"
#include "particle.hpp"

using namespace boost::python;


BOOST_PYTHON_MODULE(libsink_wrapper)
{

  class_<std::vector<float> >("float_vector")
    .def(vector_indexing_suite<std::vector<float> >());

  class_<std::vector<class Particle>>("ParticleList")
    .def(vector_indexing_suite<std::vector<class Particle>>());

  class_<Particle>("Particle", init<std::vector<float>,
		   std::vector<float>, float, float, float>())
    .def_readwrite("pos", &Particle::pos)
    .def_readwrite("vel", &Particle::vel)
    .def_readwrite("acc", &Particle::acc)
    .def("getDensity", &Particle::getDensity)
    .def("getPressure", &Particle::getPressure)
    .def("getEnergy", &Particle::getEnergy)
    ;

  class_<Engine>("Engine", init<std::vector<class Particle>, float, float, double, bool>())
    .def("doStep", &Engine::doStep)
    .def("print", &Engine::print)
    .def("makeCells", &Engine::makeCells)
    .def("disableGravity", &Engine::disableGravity)
    .def("disableHydro", &Engine::disableHydro)
    .def("disableStars", &Engine::disableStars)
    .def("disableCooling", &Engine::disableCooling)
    .def("writeSnapshot", &Engine::writeSnapshot)
    .def_readwrite("parts", &Engine::parts)
    .def_readwrite("n_parts", &Engine::n_parts)
    .def_readwrite("time", &Engine::time)
    ;
}
