#include <cmath>

#define hydro_gamma 1.66666666666666667f
#define hydro_gamma_minus_one 0.66666666666666667f
#define hydro_gamma_plus_one 2.66666666666666667f
#define hydro_one_over_gamma_minus_one 1.5f
#define hydro_gamma_plus_one_over_two_gamma 0.8f
#define hydro_gamma_minus_one_over_two_gamma 0.2f
#define hydro_gamma_minus_one_over_gamma_plus_one 0.25f
#define hydro_two_over_gamma_plus_one 0.75f
#define hydro_two_over_gamma_minus_one 3.f
#define hydro_gamma_minus_one_over_two 0.33333333333333333f
#define hydro_two_gamma_over_gamma_minus_one 5.f
#define hydro_one_over_gamma 0.6f

/**
 * @brief Returns the argument to the power given by the adiabatic index
 *
 * Computes \f$x^\gamma\f$.
 */
__attribute__((always_inline, const)) inline static float pow_gamma(float x) {

  const float cbrt = std::cbrtf(x);                 /* x^(1/3) */
  return cbrt * cbrt * x;                      /* x^(5/3) */
}

/**
 * @brief Returns the argument to the power given by the adiabatic index minus
 * one
 *
 * Computes \f$x^{(\gamma-1)}\f$.
 */
__attribute__((always_inline, const)) inline static float pow_gamma_minus_one(
    float x) {

  const float cbrt = std::cbrtf(x);                 /* x^(1/3) */
  return cbrt * cbrt;                          /* x^(2/3) */
}

/**
 * @brief Returns one over the argument to the power given by the adiabatic
 * index minus one
 *
 * Computes \f$x^{-(\gamma-1)}\f$.
 */
__attribute__((always_inline, const)) inline static float
pow_minus_gamma_minus_one(float x) {

  const float cbrt_inv = 1.f / std::cbrtf(x);       /* x^(-1/3) */
  return cbrt_inv * cbrt_inv;                  /* x^(-2/3) */
}

/**
 * @brief Returns one over the argument to the power given by the adiabatic
 * index
 *
 * Computes \f$x^{-\gamma}\f$.
 *
 * @param x Argument
 * @return One over the argument to the power given by the adiabatic index
 */
__attribute__((always_inline, const)) inline static float pow_minus_gamma(
    float x) {

  const float cbrt_inv = 1.f / std::cbrtf(x);       /* x^(-1/3) */
  const float cbrt_inv2 = cbrt_inv * cbrt_inv; /* x^(-2/3) */
  return cbrt_inv * cbrt_inv2 * cbrt_inv2;     /* x^(-5/3) */
}

/**
 * @brief Return the argument to the power given by two divided by the adiabatic
 * index minus one
 *
 * Computes \f$x^{\frac{2}{\gamma - 1}}\f$.
 *
 * @param x Argument
 * @return Argument to the power two divided by the adiabatic index minus one
 */
__attribute__((always_inline, const)) inline static float
pow_two_over_gamma_minus_one(float x) {

  return x * x * x; /* x^3 */

}

/**
 * @brief Return the argument to the power given by two times the adiabatic
 * index divided by the adiabatic index minus one
 *
 * Computes \f$x^{\frac{2\gamma}{\gamma - 1}}\f$.
 *
 * @param x Argument
 * @return Argument to the power two times the adiabatic index divided by the
 * adiabatic index minus one
 */
__attribute__((always_inline, const)) inline static float
pow_two_gamma_over_gamma_minus_one(float x) {

  const float x2 = x * x;
  const float x3 = x2 * x;
  return x2 * x3;

}

/**
 * @brief Return the argument to the power given by the adiabatic index minus
 * one  divided by two times the adiabatic index
 *
 * Computes \f$x^{\frac{\gamma - 1}{2\gamma}}\f$.
 *
 * @param x Argument
 * @return Argument to the power the adiabatic index minus one divided by two
 * times the adiabatic index
 */
__attribute__((always_inline, const)) inline static float
pow_gamma_minus_one_over_two_gamma(float x) {

  return powf(x, 0.2f); /* x^0.2 */

}

/**
 * @brief Return the inverse argument to the power given by the adiabatic index
 * plus one divided by two times the adiabatic index
 *
 * Computes \f$x^{-\frac{\gamma + 1}{2\gamma}}\f$.
 *
 * @param x Argument
 * @return Inverse argument to the power the adiabatic index plus one divided by
 * two times the adiabatic index
 */
__attribute__((always_inline, const)) inline static float
pow_minus_gamma_plus_one_over_two_gamma(float x) {

  return powf(x, -0.8f); /* x^-0.8 */
}

/**
 * @brief Return the argument to the power one over the adiabatic index
 *
 * Computes \f$x^{\frac{1}{\gamma}}\f$.
 *
 * @param x Argument
 * @return Argument to the power one over the adiabatic index
 */
__attribute__((always_inline, const)) inline static float pow_one_over_gamma(
    float x) {

  return powf(x, hydro_one_over_gamma); /* x^(3/5) */
}

/**
 * @brief Return the argument to the power three adiabatic index minus two.
 *
 * Computes \f$x^{3\gamma - 2}\f$.
 *
 * @param x Argument
 */
__attribute__((always_inline, const)) inline static float
pow_three_gamma_minus_two(float x) {

  return x * x * x; /* x^(3) */
}

/**
 * @brief Return the argument to the power three adiabatic index minus five over
 * two.
 *
 * Computes \f$x^{(3\gamma - 5)/2}\f$.
 *
 * @param x Argument
 */
__attribute__((always_inline, const)) inline static float
pow_three_gamma_minus_five_over_two(float x) {

  return 1.f; /* x^(0) */
}

__attribute__((always_inline, const)) inline static float
gas_pressure_from_entropy(float density, float entropy) {

  return entropy * pow_gamma(density);
}


__attribute__((always_inline, const)) inline static float
gas_soundspeed_from_pressure(float density, float P) {

  const float density_inv = 1.f / density;
  return sqrtf(hydro_gamma * P * density_inv);
}

/**
 * @brief Returns the entropy given density and internal energy
 *
 * Computes \f$S = \frac{(\gamma - 1)u}{\rho^{\gamma-1}}\f$.
 *
 * @param density The density \f$\rho\f$
 * @param u The internal energy \f$u\f$
 */
__attribute__((always_inline, const)) inline static float
gas_entropy_from_internal_energy(float density, float u) {

  return hydro_gamma_minus_one * u * pow_minus_gamma_minus_one(density);
}
