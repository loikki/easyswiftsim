#ifndef STARS_HPP
#define STARS_HPP

#include "constant.hpp"

/**
 * Simple power law
 */
inline float initial_mass_function() {
  const float rand_num = distribution(generator);

  const float alpha_1 = imf_alpha + 1.;

  const float mass_min_alpha_1 = 
    std::pow(mass_min, alpha_1);

  const float b = alpha_1 / (
      std::pow(mass_max, alpha_1) -
      mass_min_alpha_1);

  const float m_alpha_1 = rand_num * alpha_1 / b +
    mass_min_alpha_1;

  return std::pow(m_alpha_1, 1. / alpha_1);
  
}


#endif // STARS_HPP
