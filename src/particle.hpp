#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "constant.hpp"
#include "enum.hpp"
#include "stars.hpp"

#include <vector>
#include <iostream>

#define max_smoothing_iter 30
#define hydro_h_max 10.

#define sink_error(s, ...)						\
  ({									\
    fflush(stdout);							\
    fprintf(stderr, "%s:%s():%i: " s "\n",				\
            __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);		\
    exit(1);								\
  })


struct hydro {
  /* hydro acceleration */
  float acc[3];

  /* Smoothing length */
  float h;

  /* Density */
  float rho;

  /* Entropy at the last full step. */
  float entropy_full;

  /* Predicted velocity. */
  float v_pred[3];

  /* Entropy */
  float entropy;

  /* Entropy derivative */
  float entropy_dt;
  
  /* Redo ghost */
  bool redo;

  struct {
    /* Number of neighbours. */
    float wcount;

    /* Number of neighbours spatial derivative. */
    float wcount_dh;

    /* Derivative of the density with respect to h. */
    float rho_dh;

    /* Particle velocity curl. */
    float rot_v[3];

    /* Particle velocity divergence. */
    float div_v;

    /* Boundaries for the bissection method in the ghost */
    float left;
    float right;
    
  } density;

  struct {

    /* Balsara switch */
    float balsara;

    /*! "Grad h" term */
    float f;

    /* Pressure over density squared  */
    float P_over_rho2;

    /* Particle sound speed. */
    float soundspeed;

    /* Signal velocity. */
    float v_sig;

    /* Time derivative of the smoothing length */
    float h_dt;

  } force;

#ifdef DEBUG_INTERACTIONS_SPH
  /*! Number of interactions in the density SELF and PAIR */
  int num_ngb_density;

  /*! Number of interactions in the force SELF and PAIR */
  int num_ngb_force;
#endif

  /* Star formation */
  struct {

  } sf;

};

struct sink {
  /* Smoothing length */
  float h;

  /* birth time */
  float birth_time;

  /* Birth density */
  float birth_density;

  /* Birth entropy */
  float birth_entropy;

  /* Time transformation into star */
  float transformation_time;

  /* Mass to reach */
  float final_mass;
};

struct star {

};

enum part_type {
  type_hydro,
  type_sink,
  type_star,
};

class Engine;

class Particle {
public:
  std::vector<float> pos;
  std::vector<float> vel;
  std::vector<float> acc;
  float mass;
  int id;
  /* Field used to sort the particles in the cells */
  int sort_key;
  enum part_type type;

  union {
    struct hydro hydro;

    struct star star;

    struct sink sink;
  };

  /* Constructors */
  Particle(std::vector<float> pos, std::vector<float> vel, float mass, float h, float u);
  Particle() {};

  /* Conversion */
  void convertEntropy(class Engine *e);

  /* Hydro */
  void density(class Particle *a, float r2, float dx[3], class Engine *e);
  void force(class Particle *a, float r2, float dx[3], class Engine *e);
  void hydroEndDensity();
  void hydroPrepareForce(class Engine *e);
  void hydroResetAcceleration();
  void hydroInitPart();
  void hydroHasNoNeighbours();
  bool ghost(class Engine *e);
  float getPressure();
  void setEnergy(float energy);
  float getEnergy();
  float getTemperature();
  float getDensity();
  void hydroKickExtra(float dt);
  void hydroEndForce();
  void hydroPredictExtra(float dt, class Engine *e);
  void hydroResetPredictedValues(class Engine *e);
  float pressureFloor();

  /* Cooling */
  void cooling(float dt);

  /* Star formation */
  void sinkMerging(class Particle *pj);
  void sinkAccretion(class Particle *pj);
  void sinkEvolution();
  void sinkFormation(float dt,class Engine *e);

  /* Gravity */
  void gravInteraction(class Particle *pj, float r2, float r[3], class Engine *e);

  /* Particle updates */
  void resetAcceleration(bool with_hydro);
  float getTimeStep(class Engine *e);

  void kick(float dt, bool with_hydro);
  void drift(float dt, class Engine *e);

  /* Operators */
  void print();

  bool operator<(class Particle &b) {
    return this->sort_key < b.sort_key;
  }

  bool operator==(const class Particle& data) const {
    return this == &data;
  }   

};


#endif // PARTICLE_HPP
