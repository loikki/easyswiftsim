#include <array>
#include <climits>
#include <vector>
#include <random>


#include "engine.hpp"
#include "particle.hpp"



int main(int, char**) {

  const int N = 10000;
  const float box_size = 1.;
  const float eps = box_size * pow(N, 1./3.) / 30.;
  const float mass = 1;
  const float h = 3 * box_size / pow(10000, 1./3.);
  const float u = 1;
  const double dt_max = 100;
  const bool periodic = true;
  
  std::vector<class Particle> parts(N);

  /* Init position */
  for(int i = 0; i < N; i++) {
    std::vector<float> pos(3);
    std::vector<float> vel(3);
    for(int k = 0; k < 3; k++) {
      pos[k] = (float) std::rand() / (float) INT_MAX;
      vel[k] = 0.;
    }
    parts[i] = Particle(pos, vel, mass, h, u);
  }

  class Engine e = Engine(parts, box_size, eps, dt_max, periodic);

  e.doStep(1.);
  
  e.clean();

}
