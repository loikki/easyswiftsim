#include <cmath>
#include <cfloat>

#include "constant.hpp"
#include "particle.hpp"
#include "engine.hpp"
#include "equation_of_state.hpp"
#include "kernel.hpp"

/* Hydro */
#define hydro_eta_dim pow_dimension(hydro_eta)

#define log_max_h_change std::log(std::pow(hydro_props_default_volume_change, 1./3.))

/* Gravity */
#define kernel_gravity_softening_plummer_equivalent_inv (1. / 2.8)


/**
 * @brief Initialize the particle.
 *
 * @param pos 3D position of the particle
 * @param vel 3D velocity of the particle
 * @param mass Mass of the particle
 * @param h Smoothing length of the particle
 * @param u Internal energy of the particle
 */
Particle::Particle(std::vector<float> pos, std::vector<float> vel, float mass, float h, float u) {
  /* Save the scalar variables */
  this->hydro.h = h;
  this->hydro.entropy = u;
  this->hydro.entropy_full = u;
  this->type = type_hydro;

  this->mass = mass;

  /* Create vectors and save data */
  this->pos.resize(3);
  this->vel.resize(3);
  this->acc.resize(3);
  
  for(int i = 0; i < 3; i++) {
    this->pos[i] = pos[i];
    this->vel[i] = vel[i];
    this->hydro.v_pred[i] = vel[i];
    this->acc[i] = 0;
  }

  /* Set fields to 0 */
  this->resetAcceleration(true);
  this->hydroInitPart();

#ifdef DEBUG_INTERACTIONS_SPH
  this->hydro.num_ngb_density = 0;
  this->hydro.num_ngb_force = 0;
#endif

}

/**
 * @brief Convert the energy to entropy.
 *
 * This function is called directly after the initial density loop in
 * order to convert the energy given in input into entropy.
 *
 * @param e The #Engine
 */
void Particle::convertEntropy(class Engine *e) {

  /* Convert energy to entropy */
  this->hydro.entropy = gas_entropy_from_internal_energy(
      this->hydro.rho, this->hydro.entropy);

  this->hydro.entropy_full = this->hydro.entropy;

  /* Compute variables that depends on it */
  float pressure = gas_pressure_from_entropy(this->hydro.rho, this->hydro.entropy);
  if (e->policy & policy_grav) {
    pressure = std::max(pressure, this->pressureFloor());
  }

  /* Compute the sound speed */
  const float soundspeed = gas_soundspeed_from_pressure(this->hydro.rho, pressure);

  /* Divide the pressure by the density squared to get the SPH term */
  const float rho_inv = 1.f / this->hydro.rho;
  const float P_over_rho2 = pressure * rho_inv * rho_inv;

  this->hydro.force.soundspeed = soundspeed;
  this->hydro.force.P_over_rho2 = P_over_rho2;
}

/**
 * @brief Print the particle information.
 */
void Particle::print() {
  std::cout << "(" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")" << std::endl;
}

/**
 * @brief Compute the gravitational interaction (non symmetric).
 *
 * @param pj The particle to interact with
 * @param r2 The distance power 2
 * @param r The vector distance.
 * @param e The #Engine.
 */
void Particle::gravInteraction(class Particle *pj, float r2, float r[3], class Engine *e) {
  for(int i = 0; i < 3; i++) {
    this->acc[i] -= G * pj->mass * r[i] / pow(r2 + e->eps2, 1.5);
  }
    
}

/**
 * @brief Compute the hydro density interaction (non symmetric)
 *
 * The SPH method is Gadget-2.
 *
 * @param pj The particle to interact with
 * @param r2 The distance power 2
 * @param dx The vector distance.
 * @param e The #Engine.
 */
void Particle::density(class Particle *pj, float r2, float dx[3], class Engine *e) {
  float wi, wi_dx;
  float dv[3], curlvr[3];

  /* Get the masses. */
  const float mj = pj->mass;

  /* Get r and 1/r. */
  const float r_inv = 1.0f / sqrtf(r2);
  const float r = r2 * r_inv;

  /* Compute the kernel function */
  const float hi_inv = 1.0f / this->hydro.h;
  const float ui = r * hi_inv;
  kernel_deval(ui, &wi, &wi_dx);

  /* Compute contribution to the density */
  this->hydro.rho += mj * wi;
  this->hydro.density.rho_dh -= mj * (hydro_dimension * wi + ui * wi_dx);

  /* Compute contribution to the number of neighbours */
  this->hydro.density.wcount += wi;
  this->hydro.density.wcount_dh -= (hydro_dimension * wi + ui * wi_dx);

  const float fac = mj * wi_dx * r_inv;

  /* Compute dv dot r */
  dv[0] = this->hydro.v_pred[0] - pj->hydro.v_pred[0];
  dv[1] = this->hydro.v_pred[1] - pj->hydro.v_pred[1];
  dv[2] = this->hydro.v_pred[2] - pj->hydro.v_pred[2];
  const float dvdr = dv[0] * dx[0] + dv[1] * dx[1] + dv[2] * dx[2];
  this->hydro.density.div_v -= fac * dvdr;

  /* Compute dv cross r */
  curlvr[0] = dv[1] * dx[2] - dv[2] * dx[1];
  curlvr[1] = dv[2] * dx[0] - dv[0] * dx[2];
  curlvr[2] = dv[0] * dx[1] - dv[1] * dx[0];

  this->hydro.density.rot_v[0] += fac * curlvr[0];
  this->hydro.density.rot_v[1] += fac * curlvr[1];
  this->hydro.density.rot_v[2] += fac * curlvr[2];

#ifdef DEBUG_INTERACTIONS_SPH
  this->hydro.num_ngb_density += 1;
#endif
}

/**
 * @brief Compute the hydro force interaction (non symmetric)
 *
 * The SPH method is Gadget-2.
 *
 * @param pj The particle to interact with
 * @param r2 The distance power 2
 * @param dx The vector distance.
 * @param e The #Engine.
 */
void Particle::force(class Particle *pj, float r2, float dx[3], class Engine *e) {

  float wi, wj, wi_dx, wj_dx;

  /* Get r and 1/r. */
  const float r_inv = 1.0f / sqrtf(r2);
  const float r = r2 * r_inv;

  /* Get some values in local variables. */
  const float mj = pj->mass;
  const float rhoi = this->hydro.rho;
  const float rhoj = pj->hydro.rho;

  /* Get the kernel for hi. */
  const float hi_inv = 1.0f / this->hydro.h;
  const float hid_inv = pow_dimension_plus_one(hi_inv); /* 1/h^(d+1) */
  const float ui = r * hi_inv;
  kernel_deval(ui, &wi, &wi_dx);
  const float wi_dr = hid_inv * wi_dx;

  /* Get the kernel for hj. */
  const float hj_inv = 1.0f / pj->hydro.h;
  const float hjd_inv = pow_dimension_plus_one(hj_inv); /* 1/h^(d+1) */
  const float xj = r * hj_inv;
  kernel_deval(xj, &wj, &wj_dx);
  const float wj_dr = hjd_inv * wj_dx;

  /* Compute h-gradient terms */
  const float f_i = this->hydro.force.f;
  const float f_j = pj->hydro.force.f;

  /* Compute pressure terms */
  const float P_over_rho2_i = this->hydro.force.P_over_rho2;
  const float P_over_rho2_j = pj->hydro.force.P_over_rho2;

  /* Compute sound speeds */
  const float ci = this->hydro.force.soundspeed;
  const float cj = pj->hydro.force.soundspeed;

  /* Compute dv dot r. */
  const float dvdr =
    (this->hydro.v_pred[0] - pj->hydro.v_pred[0]) * dx[0] +
    (this->hydro.v_pred[1] - pj->hydro.v_pred[1]) * dx[1] +
    (this->hydro.v_pred[2] - pj->hydro.v_pred[2]) * dx[2];

  /* Balsara term */
  const float balsara_i = this->hydro.force.balsara;
  const float balsara_j = pj->hydro.force.balsara;

  /* Are the particles moving towards each others ? */
  const float omega_ij = std::min(dvdr, 0.f);
  const float mu_ij = r_inv * omega_ij; /* This is 0 or negative */

  /* Signal velocity */
  const float v_sig = ci + cj - const_viscosity_beta * mu_ij;

  /* Now construct the full viscosity term */
  const float rho_ij = 0.5f * (rhoi + rhoj);
  const float visc = -0.25f * v_sig * mu_ij * (balsara_i + balsara_j) / rho_ij;

  /* Now, convolve with the kernel */
  const float visc_term = 0.5f * visc * (wi_dr + wj_dr) * r_inv;
  const float sph_term =
      (f_i * P_over_rho2_i * wi_dr + f_j * P_over_rho2_j * wj_dr) * r_inv;

  /* Eventually got the acceleration */
  const float acc = visc_term + sph_term;

  /* Use the force Luke ! */
  this->hydro.acc[0] -= mj * acc * dx[0];
  this->hydro.acc[1] -= mj * acc * dx[1];
  this->hydro.acc[2] -= mj * acc * dx[2];
  
  /* Get the time derivative for h. */
  this->hydro.force.h_dt -= mj * dvdr * r_inv / rhoj * wi_dr;

  /* Update the signal velocity. */
  this->hydro.force.v_sig = std::max(this->hydro.force.v_sig, v_sig);

  /* Change in entropy */
  this->hydro.entropy_dt += mj * visc_term * dvdr;

#ifdef DEBUG_INTERACTIONS_SPH
  this->hydro.num_ngb_force += 1;
#endif
}


/**
 * @brief Compute the required time step.
 *
 * @param e The #Engine.
 *
 * @return The time step.
 */
float Particle::getTimeStep(class Engine *e) {
  /* Gravity */
  float acc = 0;
  for(int i = 0; i < 3; i++) {
    float tmp = this->acc[i] + this->hydro.acc[i];
    acc += tmp * tmp;
  }

  const float ac_inv = (acc > 0.f) ? 1.f / std::sqrt(acc) : FLT_MAX;

  float dt = 2 * kernel_gravity_softening_plummer_equivalent_inv;
  dt *= grav_eta * std::sqrt(e->eps2) * ac_inv;

  dt = std::sqrt(dt);

  /* Skip hydro if not used */
  if (!(e->policy & policy_hydro))
    return dt;

  /* Hydro */
  const float dt_cfl = 2.f * kernel_gamma * CFL * this->hydro.h /
    (this->hydro.force.v_sig);

  /* Smoothing length */
  const float dt_h_change =
      (this->hydro.force.h_dt != 0.0f)
    ? std::abs(log_max_h_change * this->hydro.h / this->hydro.force.h_dt)
          : FLT_MAX;

  if (dt_cfl == 0. || dt == 0. || dt_h_change == 0.) {
    sink_error("Null time step %g, %g, %g", dt_cfl, dt, dt_h_change);
  }
  return std::min(dt, dt_cfl);
}

/**
 * @brief Drift a particle in time.
 *
 * @param dt The time step.
 * @param e The #Engine
 */
void Particle::drift(float dt, class Engine *e) {
  for(int i = 0; i < 3; i++) {
    this->pos[i] += this->vel[i] * dt;

    /* Apply periodic condition */
    if (e->periodic) {
      if (this->pos[i] < 0) {
	this->pos[i] += e->box_size;
      }
      if (this->pos[i] >= e->box_size) {
	this->pos[i] -= e->box_size;
      }
    }
  }

  /* Skip hydro? */
  if (!(e->policy & policy_hydro) || this->type != type_hydro)
    return;

  /* And now the hydro */
  this->hydroPredictExtra(dt, e);
}

/**
 * @brief Kick a particle in time.
 *
 * @param dt The time step.
 * @param with_hydro Compute the hydro too?
 */
void Particle::kick(float dt, bool with_hydro) {
  for(int i = 0; i < 3; i++) {
    float tmp = this->acc[i] + this->hydro.acc[i];
    this->vel[i] += tmp * dt;
  }

  if (!with_hydro || this->type != type_hydro)
    return;
  
  /* And now the hydro */
  this->hydroKickExtra(dt);
}


/**
 * @brief Reset the acceleration fields.
 *
 * @param with_hydro Compute the hydro too?
 */
void Particle::resetAcceleration(bool with_hydro) {
  for(int i = 0; i < 3; i++) {
    this->acc[i] = 0;
  }

  if (!with_hydro)
    return;
  
  /* And now the hydro */
  this->hydroResetAcceleration();
}

/**
 * @brief Compute a ghost iterations.
 *
 * @param e The #Engine.
 * @return true if need to be recomputed.
 */
bool Particle::ghost(class Engine *e) {

  /* Get some useful values */
  const float h_old = this->hydro.h;
  const float h_old_dim = pow_dimension(h_old);
  const float h_old_dim_minus_one = pow_dimension_minus_one(h_old);

  float h_new;
  int has_no_neighbours = 0;

  if (this->hydro.density.wcount == 0.f) { /* No neighbours case */

    /* Flag that there were no neighbours */
    has_no_neighbours = 1;

    /* Double h and try again */
    h_new = 2.f * h_old;

  } else {

    /* Finish the density calculation */
    this->hydroEndDensity();

    /* Compute one step of the Newton-Raphson scheme */
    const float n_sum = this->hydro.density.wcount * h_old_dim;
    const float n_target = hydro_eta_dim;
    const float f = n_sum - n_target;
    const float f_prime =
      this->hydro.density.wcount_dh * h_old_dim +
      hydro_dimension * this->hydro.density.wcount * h_old_dim_minus_one;
    
    /* Improve the bisection bounds */
    if (n_sum < n_target)
      this->hydro.density.left = std::max(this->hydro.density.left, h_old);
    else if (n_sum > n_target)
      this->hydro.density.right = std::min(this->hydro.density.right, h_old);
    
    /* Skip if h is already h_max and we don't have enough neighbours */
    /* Same if we are below h_min */
    if (((this->hydro.h >= hydro_h_max) && (f < 0.f)) ||
	((this->hydro.h <= hydro_h_min) && (f > 0.f))) {
      
      /* We have a particle whose smoothing length is already set (wants
       * to be larger but has already hit the maximum OR wants to be
       * smaller but has already reached the minimum). So, just tidy up as
       * if the smoothing length had converged correctly  */

      /* As of here, particle force variables will be set. */
      
      /* Compute variables required for the force loop */
      this->hydroPrepareForce(e);

      /* The particle force values are now set.  Do _NOT_
	 try to read any particle density variables! */

      /* Prepare the particle for the force loop over neighbours */
      this->hydroResetAcceleration();
      
      /* Ok, we are done with this particle */
      this->hydro.redo = false;
      return false;
    }

    /* Normal case: Use Newton-Raphson to get a better value of h */
    
    /* Avoid floating point exception from f_prime = 0 */
    h_new = h_old - f / (f_prime + FLT_MIN);

    /* Be verbose about the particles that struggle to converge */
    if (e->step_ghost > max_smoothing_iter - 10) {

      std::cout << "Smoothing length convergence problem: iter=" << e->step_ghost
		<< ", h = " << this->hydro.h << std::endl;
      // message(
      // 	      "Smoothing length convergence problem: iter=%d p->id=%lld "
      // 	      "h_init=%12.8e h_old=%12.8e h_new=%12.8e f=%f f_prime=%f "
      // 	      "n_sum=%12.8e n_target=%12.8e left=%12.8e right=%12.8e",
      // 	      num_reruns, p->id, h_init, h_old, h_new, f, f_prime, n_sum,
      // 	      n_target, left[i], right[i]);
    }

    /* Safety check: truncate to the range [ h_old/2 , 2h_old ]. */
    h_new = std::min(h_new, 2.f * h_old);
    h_new = std::max(h_new, 0.5f * h_old);

    /* Verify that we are actually progrssing towards the answer */
    h_new = std::max(h_new, this->hydro.density.left);
    h_new = std::min(h_new, this->hydro.density.right);
  }

  /* Check whether the particle has an inappropriate smoothing length */
  if (fabsf(h_new - h_old) > h_tolerance * h_old) {

    /* Ok, correct then */
    
    /* Case where we have been oscillating around the solution */
    if ((h_new == this->hydro.density.left && h_old == this->hydro.density.right) ||
	(h_old == this->hydro.density.left && h_new == this->hydro.density.right)) {

      /* Bissect the remaining interval */
      this->hydro.h = pow_inv_dimension(0.5f * (
          pow_dimension(this->hydro.density.left) + pow_dimension(this->hydro.density.right)));

    } else {

      /* Normal case */
      this->hydro.h = h_new;
    }

    /* If within the allowed range, try again */
    if (this->hydro.h < hydro_h_max && this->hydro.h > hydro_h_min) {

      /* Re-initialise everything */
      this->hydroInitPart();

      /* Off we go ! */
      this->hydro.redo = true;
      return true;

    } else if (this->hydro.h <= hydro_h_min) {
      
      /* Ok, this particle is a lost cause... */
      this->hydro.h = hydro_h_min;
   
    } else if (this->hydro.h >= hydro_h_max) {

      /* Ok, this particle is a lost cause... */
      this->hydro.h = hydro_h_max;
	    
      /* Do some damage control if no neighbours at all were found */
      if (has_no_neighbours) {
	this->hydroHasNoNeighbours();
      }
   
    } else {
      sink_error(
	    "Fundamental problem with the smoothing length iteration "
	    "logic.");
    }
  }

  /* We now have a particle whose smoothing length has converged */

  /* As of here, particle force variables will be set. */  

  /* Compute variables required for the force loop */
  this->hydroPrepareForce(e);

  /* The particle force values are now set.  Do _NOT_
     try to read any particle density variables! */

  /* Prepare the particle for the force loop over neighbours */
  this->hydroResetAcceleration();

  this->hydro.redo = false;
  return false;
}


/**
 * @brief Transform the density variables into the required fields.
 */
void Particle::hydroEndDensity() {

  /* Some smoothing length multiples. */
  const float h = this->hydro.h;
  const float h_inv = 1.0f / h;                       /* 1/h */
  const float h_inv_dim = pow_dimension(h_inv);       /* 1/h^d */
  const float h_inv_dim_plus_one = h_inv_dim * h_inv; /* 1/h^(d+1) */

  /* Final operation on the density (add self-contribution). */
  this->hydro.rho += this->mass * kernel_root;
  this->hydro.density.rho_dh -= hydro_dimension * this->mass * kernel_root;
  this->hydro.density.wcount += kernel_root;
  this->hydro.density.wcount_dh -= hydro_dimension * kernel_root;

  /* Finish the calculation by inserting the missing h-factors */
  this->hydro.rho *= h_inv_dim;
  this->hydro.density.rho_dh *= h_inv_dim_plus_one;
  this->hydro.density.wcount *= h_inv_dim;
  this->hydro.density.wcount_dh *= h_inv_dim_plus_one;

  const float rho_inv = 1.f / this->hydro.rho;

  /* Finish calculation of the (physical) velocity curl components */
  this->hydro.density.rot_v[0] *= h_inv_dim_plus_one * rho_inv;
  this->hydro.density.rot_v[1] *= h_inv_dim_plus_one * rho_inv;
  this->hydro.density.rot_v[2] *= h_inv_dim_plus_one * rho_inv;

  /* Finish calculation of the (physical) velocity divergence */
  this->hydro.density.div_v *= h_inv_dim_plus_one * rho_inv;

}

/**
 * @brief Compute the required fields for the force.
 *
 * @param e The #Engine
 */
void Particle::hydroPrepareForce(class Engine *e) {

  /* Inverse of the co-moving density */
  const float rho_inv = 1.f / this->hydro.rho;

  /* Inverse of the smoothing length */
  const float h_inv = 1.f / this->hydro.h;

  /* Compute the norm of the curl */
  const float curl_v = sqrtf(this->hydro.density.rot_v[0] * this->hydro.density.rot_v[0] +
                             this->hydro.density.rot_v[1] * this->hydro.density.rot_v[1] +
                             this->hydro.density.rot_v[2] * this->hydro.density.rot_v[2]);

  /* Compute the norm of div v including the Hubble flow term */
  const float div_physical_v = this->hydro.density.div_v;
  const float abs_div_physical_v = fabsf(div_physical_v);

  /* Compute the pressure */
  float pressure = gas_pressure_from_entropy(this->hydro.rho, this->hydro.entropy);
  if (e->policy & policy_grav) {
    pressure = std::max(pressure, this->pressureFloor());
  }

  /* Compute the sound speed */
  const float soundspeed = gas_soundspeed_from_pressure(this->hydro.rho, pressure);

  /* Divide the pressure by the density squared to get the SPH term */
  const float P_over_rho2 = pressure * rho_inv * rho_inv;

  /* Compute the Balsara switch */
  /* Pre-multiply in the AV factor; hydro_props are not passed to the iact
   * functions */
  const float balsara = hydro_viscosity_alpha * abs_div_physical_v /
                        (abs_div_physical_v + curl_v +
                         0.0001f * soundspeed * h_inv);

  /* Compute the "grad h" term */
  const float omega_inv =
      1.f / (1.f + this->hydro.h * this->hydro.density.rho_dh * rho_inv / hydro_dimension);

  /* Update variables. */
  this->hydro.force.f = omega_inv;
  this->hydro.force.P_over_rho2 = P_over_rho2;
  this->hydro.force.soundspeed = soundspeed;
  this->hydro.force.balsara = balsara;

}

/**
 * @brief Reset the hydro acceleration
 */
void Particle::hydroResetAcceleration() {
  /* Reset the acceleration. */
  this->hydro.acc[0] = 0.0f;
  this->hydro.acc[1] = 0.0f;
  this->hydro.acc[2] = 0.0f;

  /* Reset the time derivatives. */
  this->hydro.entropy_dt = 0.0f;
  this->hydro.force.h_dt = 0.0f;

  /* Reset maximal signal velocity */
  this->hydro.force.v_sig = this->hydro.force.soundspeed;

#ifdef DEBUG_INTERACTIONS_SPH
  this->hydro.num_ngb_force = 0;
#endif
}

/**
 * @brief Initialize the hydro density fields
 */
void Particle::hydroInitPart() {
  this->hydro.rho = 0.f;
  this->hydro.density.wcount = 0.f;
  this->hydro.density.wcount_dh = 0.f;
  this->hydro.density.rho_dh = 0.f;
  this->hydro.density.div_v = 0.f;
  this->hydro.density.rot_v[0] = 0.f;
  this->hydro.density.rot_v[1] = 0.f;
  this->hydro.density.rot_v[2] = 0.f;
  this->hydro.density.left = hydro_h_min;
  this->hydro.density.right = hydro_h_max;
#ifdef DEBUG_INTERACTIONS_SPH
  this->hydro.num_ngb_density = 0;
#endif
}

/**
 * @brief Set the variables to correct values if no neighbours
 */
void Particle::hydroHasNoNeighbours() {
  /* Some smoothing length multiples. */
  const float h = this->hydro.h;
  const float h_inv = 1.0f / h;                 /* 1/h */
  const float h_inv_dim = pow_dimension(h_inv); /* 1/h^d */

  /* Re-set problematic values */
  this->hydro.rho = this->mass * kernel_root * h_inv_dim;
  this->hydro.density.wcount = kernel_root * h_inv_dim;
  this->hydro.density.rho_dh = 0.f;
  this->hydro.density.wcount_dh = 0.f;
  this->hydro.density.div_v = 0.f;
  this->hydro.density.rot_v[0] = 0.f;
  this->hydro.density.rot_v[1] = 0.f;
  this->hydro.density.rot_v[2] = 0.f;
}


/**
 * @brief Compute the internal energy.
 *
 * @return The internal energy
 */
float Particle::getEnergy() {
  if (this->type != type_hydro)
    return -1;

  return this->hydro.entropy * std::pow(this->hydro.rho, hydro_gamma - 1.)
    / (hydro_gamma - 1.);
}


/**
 * @brief Compute the temperature.
 *
 * @return The temperature
 */
float Particle::getTemperature() {
  return this->getEnergy() * (hydro_gamma - 1.) * mumh_kb;
}

/**
 * @brief Set the internal energy.
 *
 * @param The new internal energy
 */
void Particle::setEnergy(float energy) {
  if (energy < min_u) {
    energy = min_u;
  }
  this->hydro.entropy = gas_entropy_from_internal_energy(this->hydro.rho, energy);
}

/**
 * @brief Compute the pressure.
 *
 * @return The pressure
 */
float Particle::getPressure() {
  return this->hydro.entropy * pow_gamma(this->hydro.rho);
}

/**
 * @brief Compute the density.
 *
 * @return The density
 */
float Particle::getDensity() {
  return this->hydro.rho;
}



/**
 * @brief Kick the hydro quantities
 */
void Particle::hydroKickExtra(float dt) {

  /* Integrate the entropy forward in time */
  const float delta_entropy = this->hydro.entropy_dt * dt;

  /* Do not decrease the entropy by more than a factor of 2 */
  this->hydro.entropy_full =
    std::max(this->hydro.entropy_full + delta_entropy, 0.5f * this->hydro.entropy);

  /* Check against entropy floor */
  // const float floor_A = entropy_floor(p, cosmo, floor_props);

  /* Conversion done in physical space; multiplication by a3_inv is faster
   * than division by a_factor_internal_energy to do in comoving space */
  const float min_A =
      gas_entropy_from_internal_energy(this->hydro.rho, min_u);

  // /* Take highest of both limits */
  const float entropy_min = min_A; // max(min_A, floor_A);

  if (this->hydro.entropy_full < entropy_min) {
    this->hydro.entropy_full = entropy_min;
    this->hydro.entropy_dt = 0.f;
  }
}

/**
 * @brief Compute the required fields from the force interaction
 */
void Particle::hydroEndForce() {

  this->hydro.force.h_dt *= this->hydro.h / 3.;

  this->hydro.entropy_dt =
    0.5f * gas_entropy_from_internal_energy(this->hydro.rho,
					    this->hydro.entropy_dt);

}

/**
 * @brief Reset the predicted hydro values.
 *
 * @param e The #Engine
 */
void Particle::hydroResetPredictedValues(class Engine *e) {

  /* Re-set the predicted velocities */
  this->hydro.v_pred[0] = this->vel[0];
  this->hydro.v_pred[1] = this->vel[1];
  this->hydro.v_pred[2] = this->vel[2];

  /* Re-set the entropy */
  this->hydro.entropy = this->hydro.entropy_full;

  /* Re-compute the pressure */
  float pressure = gas_pressure_from_entropy(this->hydro.rho, this->hydro.entropy);
  if (e->policy & policy_grav) {
    pressure = std::max(pressure, this->pressureFloor());
  }

  /* Compute the new sound speed */
  const float soundspeed = gas_soundspeed_from_pressure(this->hydro.rho, pressure);

  /* Divide the pressure by the density squared to get the SPH term */
  const float rho_inv = 1.f / this->hydro.rho;
  const float P_over_rho2 = pressure * rho_inv * rho_inv;

  /* Update variables */
  this->hydro.force.soundspeed = soundspeed;
  this->hydro.force.P_over_rho2 = P_over_rho2;
}


/**
 * @brief Predict the hydro variables.
 *
 * @param e The #Engine
 */
void Particle::hydroPredictExtra(float dt, class Engine *e) {

  /* Predict the entropy */
  this->hydro.entropy += this->hydro.entropy_dt * dt;

  // /* Check against entropy floor */
  // const float floor_A = entropy_floor(p, cosmo, floor_props);

  /* Conversion done in physical space; multiplication by a3_inv is faster
   * than division by a_factor_internal_energy to do in comoving space */
  const float min_A =
      gas_entropy_from_internal_energy(this->hydro.rho, min_u);

  // this->hydro.entropy = max(this->hydro.entropy, floor_A);
  this->hydro.entropy = std::max(this->hydro.entropy, min_A);

  const float h_inv = 1.f / this->hydro.h;

  /* Predict smoothing length */
  const float w1 = this->hydro.force.h_dt * h_inv * dt;
  this->hydro.h *= expf(w1);

  /* Predict density */
  const float w2 = -hydro_dimension * w1;
  this->hydro.rho *= expf(w2);

  /* Re-compute the pressure */
  float pressure = gas_pressure_from_entropy(this->hydro.rho, this->hydro.entropy);
  if (e->policy & policy_grav) {
    pressure = std::max(pressure, this->pressureFloor());
  }

  /* Compute the new sound speed */
  const float soundspeed = gas_soundspeed_from_pressure(this->hydro.rho, pressure);

  /* Divide the pressure by the density squared to get the SPH term */
  const float rho_inv = 1.f / this->hydro.rho;
  const float P_over_rho2 = pressure * rho_inv * rho_inv;

  /* Update variables */
  this->hydro.force.soundspeed = soundspeed;
  this->hydro.force.P_over_rho2 = P_over_rho2;
}

float Particle::pressureFloor() {
  float floor =
    4.0 * M_1_PI * G * this->hydro.h * this->hydro. h * \
    this->hydro.rho * std::pow(n_jeans, 2./3.);
  floor *= this->hydro.rho * hydro_one_over_gamma;

  return floor;
}


void Particle::sinkMerging(class Particle *pj) {}
void Particle::sinkAccretion(class Particle *pj) {}
void Particle::sinkEvolution() {}

void Particle::sinkFormation(float dt, class Engine *e) {
  /* Check if infalling particles */
  if (this->hydro.density.div_v > 0) {
    return;
  }

  /* Check if the resolution is suffisant */
  if (this->hydro.h > e->r_acc) {
    return;
  }

  /* Check if below jeans criterion */
  float rho_jeans = 0.25 * hydro_gamma * (hydro_gamma - 1.) * M_PI * this->getEnergy();
  rho_jeans /= G * this->hydro.h * this->hydro.h * std::pow(n_jeans, 2./3.);

  if (this->hydro.rho < rho_jeans)
    return;

  /* Now we can try to transform the particle */
  const float t_ff = sqrtf(this->hydro.rho * 32. * G / (3. * M_PI));
  const float prop = 1. - std::exp(- e->c_star * dt / t_ff);

  const float rand_num = distribution(generator);

  /* Are we failing to create a sink particle? */
  if (rand_num > prop)
    return;

  std::cout << "converting particle" << std::endl;

  const float entropy = this->hydro.entropy_full;
  const float density = this->hydro.rho;

  /* From now, the struct hydro does not exist */
  
  this->type = type_sink;
  this->sink.h = e->r_acc;
  this->sink.birth_density = density;
  this->sink.birth_entropy = entropy;
  this->sink.birth_time = e->time;
  this->sink.transformation_time = e->time + t_ff;
  this->sink.final_mass = initial_mass_function();
}

void Particle::cooling(float dt) {
  const float temperature = this->getTemperature();
  const float energy = this->getEnergy();

  double cooling_rate;

  if (temperature > temperature_threshold_uv) {
    cooling_rate = (temperature > temperature_threshold) ?
      high_cooling_rate : low_cooling_rate;
  }
  else {
    cooling_rate = UV_cooling;
  }

  const double delta_u = cooling_rate * this->hydro.rho * dt;

  this->setEnergy(energy - delta_u);
  
}
