#ifndef CONSTANT_HPP
#define CONSTANT_HPP

#include <random>

/* Cooling */
/* Lambda */
static const double high_cooling_rate = 1e-8; // kpc5 / (Myr3 solMass)
static const double low_cooling_rate = 1e-13; // kpc5 / (Myr3 solMass)
static const double UV_cooling = -1e-13; // kpc5 / (Myr3 solMass)
static const float temperature_threshold = 1e4;
static const float temperature_threshold_uv = 1e2;
/* Constant in the relation u(T) */
#define mumh_kb 1.158e8 // K kg / J

/* Gravity */
static const float G = 4.498e-12; // kpc^3 / (solMass Myr^2)
/* Pressure floor */
static const float n_jeans = 10.;

/* Stars */
#define imf_alpha -2.3
#define mass_min 0.08
#define mass_max 50

/* Time step criterion */
#define grav_eta 0.025


/* Hydro */
/* Time step criterion (smoothing length) */
#define  hydro_props_default_volume_change 1.4f
/* Minimal smoothing length */
#define hydro_h_min 0
/* Tolerance on the error of the smoothing length */
#define h_tolerance 1e-4
/* Artificial viscosity parameter */
#define hydro_viscosity_alpha 0.8f
/* Artificial viscosity parameter */
#define const_viscosity_beta 3.0f
/* CFL parameter */
#define CFL 0.1
/* Minimal internal energy */
#define min_u 0
/* number of neighbors (see swift) */
#define hydro_eta 1.2348


/* Random numbers */
static const int random_seed = 10;
static std::default_random_engine generator(random_seed);
static std::uniform_real_distribution<double> distribution(0.0,1.0);


#endif // CONSTANT_HPP
