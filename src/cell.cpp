#include "cell.hpp"
#include "engine.hpp"
#include "kernel.hpp"

#include <algorithm>
#include <cmath>
#include <float.h>

/**
 * @brief Initialize a cell
 *
 * @param cell_size The length of the cell
 * @param pos The corner the closest to 0
 * @param parts The engine vector of parts (without offset)
 */
Cell::Cell(float cell_size, float pos[3], std::vector<class Particle> *parts) {
  /* Set scalar variables */
  this->is_leaf = true;
  this->parts = parts;
  this->n_parts = 0;
  this->cell_size = cell_size;
  this->depth = 0;

  /* Set vector variables */
  for(int i = 0; i < 3; i++) {
    this->pos[i] = pos[i];
  }

  /* Set children */
  for(int i = 0; i < 8; i++) {
    this->children[i] = NULL;
  }

}

/**
 * @brief Check if a particle is inside this cell
 *
 * @return True if inside cell
 */
bool Cell::inCell(class Particle *p) {

  /* Check if in this cell */
  for(int i = 0; i < 3; i++) {
    if (p->pos[i] < this->pos[i])
      return false;
    if (p->pos[i] >= this->pos[i] + this->cell_size)
      return false;
  }

  return true;
}


/**
 * @brief Compute the center of mass of the cell.
 */
void Cell::computeCenterMass() {
  /* Reset values */
  this->mass = 0;

  for(int i = 0; i < 3; i++) {
    this->cm[i] = 0;
  }

  if (this->is_leaf) {
    /* Do we have particles in it? */
    if (this->n_parts == 0)
      return;

    /* Compute the center of mass */
    for(size_t i = 0; i < this->n_parts; i++) {
      /* Get the particle */
      size_t index = i + this->start;
      class Particle *p = &(*this->parts)[index];

      /* Update the mass / CM */
      this->mass += p->mass;
      for(int k = 0; k < 3; k++) {
	this->cm[k] += p->mass * p->pos[k];
      }
    }

    /* Normalize the CM */
    for(int i = 0; i < 3; i++) {
      this->cm[i] /= this->mass;
    }
    
  }
  else {
    /* Loop over children */
    for(int i = 0; i < 8; i++) {
      class Cell *child = this->children[i];

      /* Compute child CM */
      child->computeCenterMass();

      /* Update this CM */
      this->mass += child->mass;
      for(int k = 0; k < 3; k++) {
	this->cm[k] += child->mass * child->cm[k];
      }
    }

    /* Normalize this CM */
    for(int i = 0; i < 3; i++) {
      this->cm[i] /= this->mass;
    }
  }

}

/**
 * @brief sort the particle in this cell and children
 */
void Cell::sort() {
  if (this->is_leaf)
    return;
  
  /* Tag the particles according to the cells */
  for(size_t i = 0; i < this->n_parts; i++) {
    int found = false;

    /* Get the particle */
    int index = i + this->start;
    class Particle *p = &(*this->parts)[index];

    /* Check each cell */
    for(int j = 0; j < 8; j++) {
      if (this->children[j]->inCell(p)) {
	p->sort_key = j;
	found = true;
	break;
      }
    }
    /* Check if inside a cell */
    if (!found) {
      sink_error("not found %zi: %g %g %g -> %g %g %g: %g",
		 i, p->pos[0], p->pos[1], p->pos[2],
		 this->pos[0], this->pos[1], this->pos[2], this->cell_size);
    }
  }

  /* Sort the array */
  auto beg = this->parts->begin() + this->start;
  std::sort(beg, beg + this->n_parts);

  /* assign arrays to cells */
  size_t i = 0;
  for(int c = 0; c < 8; c++) {
    /* Get the cell and particles */
    class Cell *cell = this->children[c];
    int index = i + this->start;
    class Particle *p = &(*this->parts)[index];

    /* Count number particles */
    cell->start = i;
    while (i < this->n_parts && p->sort_key <= c) {
      p += 1;
      i++;
    }

    /* Set the variables */
    int n_parts = i - cell->start;
    cell->start += this->start;
    cell->n_parts = n_parts;
  }

}

/**
 * @brief Clean the cell architecture
 */
void Cell::clean() {
  /* Remove the particles */
  this->start = 0;
  this->n_parts = 0;

  /* Nothing to do if leaf */
  if (this->is_leaf) {
    return;
  }

  /* Clean the children */
  for(int i = 0; i < 8; i++) {
    class Cell *c = this->children[i];

    /* Clean children's children */
    if (!c->is_leaf) {
      c->clean();
    }

    /* Delete children */
    delete c;
    this->children[i] = NULL;
    this->is_leaf = true;
  }
}

/**
 * @brief Get the maximal smoothing length of the particles.
 */
void Cell::getHmax() {
  this->h_max = 0;

  /* If leaf check directly the particles */
  if (this->is_leaf) {
    for(size_t i = 0; i < this->n_parts; i++) {
      size_t index = this->start + i;
      class Particle *p =  &(*this->parts)[index];

      /* Skip non hydro particles */
      if (p->type != type_hydro)
	continue;
      
      float h = p->hydro.h;
      if (h > this->h_max) {
	this->h_max = h;
      }
    }
  }

  /* If not leaf get h_max from children */
  else {
    for(int i = 0; i < 8; i++) {
      class Cell *c = this->children[i];
      c->getHmax();
      if(c->h_max > this->h_max) {
	this->h_max = c->h_max;
      }
    }
  }
}

/*
 * @brief Crete the cell hierarchy.
 */
void Cell::splitCell() {
  /* Reset the ghost counter */
  this->ghost_todo = this->n_parts;

  /* Leaf case */
  if (this->is_leaf) {
    /* Check if need to split more */
    if (this->n_parts > max_part_per_cell) {
      this->is_leaf = false;

      float sub_size = this->cell_size * 0.5;
      /* Split the cell */
      for(int i = 0; i < 8; i++) {

	/* Create new cell */
	int a = i;
	int b, c;
	get_cell_index(&a, &b, &c, 2);
	float pos[3] = {
	  this->pos[0] + sub_size * a,
	  this->pos[1] + sub_size * b,
	  this->pos[2] + sub_size * c};
	this->children[i] = new class Cell(sub_size, pos, this->parts);
	this->children[i]->depth = this->depth + 1;
      }

      /* Sort the particles into the correct children */
      this->sort();
    }
  }

  /* loop over children */
  for(int i = 0; i < 8; i++) {
    if (this->children[i]) {
      this->children[i]->splitCell();
    }
  }
}

/**
 * @brief Print a cell and children.
 */
void Cell::print() {
  std::cout << "Cell: (" << this->pos[0] << ", " << this->pos[1];
  std::cout << ", " << this->pos[2] << ")";
  std::cout << " - " << this->cell_size << std::endl;

  if (this->is_leaf) {
    for(size_t i = 0; i < this->n_parts; i++) {
      int index = i + this->start;
      class Particle *p = &(*this->parts)[index];
      std::cout << "Parts - " << p->id << ": (" << p->pos[0] << ", ";
      std::cout << p->pos[1] << ", ";
      std::cout << p->pos[2] << ")" << std::endl;
      
    }
  }
  else {
    for(int i = 0; i < 8; i++) {
      this->children[i]->print();
    }
  }

  std::cout << std::endl;
}

/**
 * @brief Do a gravity interaction between a cell and the particles (non symmetric).
 *
 * @param c The #cell that the particles interact with
 * @param e The #Engine
 */
void Cell::gravityInteractCellLevel(class Cell *c, class Engine *e) {

  /* Loop over particles */
  for(size_t i = 0; i < this->n_parts; i++) {
    /* Get current particle */
    size_t index = i + this->start;
    class Particle *p = &(*this->parts)[index];

    /* Compute distance */
    float r[3];
    float r2 = 0;
    for(int k = 0; k < 3; k++) {
      r[k] = c->cm[k] - p->pos[k];

      /* Apply periodic boundaries */
      if (e->periodic) {
	if (r[k] >= 0.5 * e->box_size) {
	  r[k] -= e->box_size;
	}
	else if (r[k] < -0.5 * e->box_size) {
	  r[k] += e->box_size;
	}
      }

      r2 += r[k] * r[k];
    }

    /* Compute the acceleration */
    for(int k = 0; k < 3; k++) {
      p->acc[k] += G * c->mass * r[k] / pow(r2 + e->eps2, 1.5);
    }
  }
}


/**
 * @brief Compute the gravitational acceleration due to cell c (non symmetric interaction)
 *
 * @param c The #Cell to interact with
 * @param e The #Engine
 */
void Cell::gravityInteractCells(class Cell *c, class Engine *e) {
  /* Recurse to children */
  if (this->gravityShouldRecurse(c, e)) {
    /* TODO remove this (due to OMP) */
    class Cell *children[8];
    for(int i = 0; i < 8; i++) {
      children[i] = this->children[i];
    }

#pragma omp parallel
    {
#pragma omp single
      {
	for(int i = 0; i < 8; i++) {
	  for(int j = 0; j < 8; j++) {
#pragma omp task depend(in: children[i]) depend(out: children[i])
	    children[i]->gravityInteractCells(c->children[j], e);
	  }
	}
      }
    }
  }

  /* Compute the accelerations */
  else {

    /* Compute through a cell - particle interaction */
    if (this->gravityCanInteractCells(c, e)) {
      this->gravityInteractCellLevel(c, e);
    }
    else {
      /* Compute through a particle - particle interaction */
      for(size_t i = 0; i < this->n_parts; i++) {
	/* Get the first particle */
	size_t ii = i + this->start;
	class Particle *pi = &(*this->parts)[ii];

	/* Loop over particles in second cell */
	for(size_t j = 0; j < c->n_parts; j++) {
	  size_t jj = j + c->start;
	  /* Check if self interaction */
	  if (jj == ii) {
	    continue;
	  }

	  /* Get the second particle */
	  class Particle *pj = &(*c->parts)[jj];

	  /* Get distances */
	  float r[3];
	  float r2 = 0;
	  for(int k = 0; k < 3; k++) {
	    r[k] = pi->pos[k] - pj->pos[k];

	    /* Apply periodic boundaries */
	    if (e->periodic) {
	      if (r[k] >= 0.5 * e->box_size) {
		r[k] -= e->box_size;
	      }
	      else if (r[k] < -0.5 * e->box_size) {
		r[k] += e->box_size;
	      }
	    }

	    r2 += r[k] * r[k];
	  }

	  /* Compute the acceleration */
	  pi->gravInteraction(pj, r2, r, e);
	}
      }
    }
  }
}


/**
 * @brief Check if two cells can interact with the hydro.
 *
 * @param c The #Cell to interact with
 * @param e The #Engine
 *
 * @return true if they can interact
 */
bool Cell::hydroCanInteract(class Cell *c, class Engine *e) {
  float r2 = 0;

  /* Compute the distance between the cells */
  for(int i = 0; i < 3; i++) {
    float dx = this->pos[i] - c->pos[i];

    /* Apply periodic boundaries */
    if (e->periodic) {
      if (dx >= 0.5 * e->box_size) {
	dx -= e->box_size;
      }
      else if (dx < -0.5 * e->box_size) {
	dx += e->box_size;
      }
    }

    r2 += dx * dx;
  }

  const float r = std::sqrt(r2);

  /* Get the lengths */
  const float h = std::max(this->h_max, c->h_max);
  const float cells_diag = std::sqrt(3.) * this->cell_size;

  /* Check the distances */
  if (r - cells_diag  > h) {
    return false;
  }
  else {
    return true;
  }
}

/**
 * @brief Check if the cell should recurse for the hydro interaction
 *
 * @param c The #Cell to interact with
 * @param e The #Engine
 */
bool Cell::hydroShouldRecurse(class Cell *c, class Engine *e) {
  if (this->is_leaf || c->is_leaf) {
    return false;
  }
  else {
    return true;
  }
}


/**
 * @brief Compute the hydro interaction with cell c (non symmetric)
 *
 * @param c The #Cell to interact with
 * @param type The interaction type
 * @param e The #Engine
 */
void Cell::hydroInteractCells(class Cell *c, int type, class Engine *e) {
  /* Are the cells close enough */
  if (!this->hydroCanInteract(c, e)) {
    return;
  }

  /* Recurse to children */
  if (this->hydroShouldRecurse(c, e)) {
    /* TODO remove this (due to OMP) */
    class Cell *children[8];
    for(int i = 0; i < 8; i++) {
      children[i] = this->children[i];
    }

#pragma omp parallel
    {
#pragma omp single
      for(int i = 0; i < 8; i++) {
	for(int j = 0; j < 8; j++) {
#pragma omp task depend(in: children[i]) depend(out: children[i])
	  children[i]->hydroInteractCells(c->children[j], type, e);
	}
      }
    }
  }

  /* Direct computation */
  else {
    /* Skip interaction if ghost does not require the computation */
    if ((type & interaction_ghost) && (this->ghost_todo == 0)) {
      return;
    }

    /* Do the direct particle-particle interactions */
    for(size_t i = 0; i < this->n_parts; i++) {
      /* Get the first particle */
      size_t ii = i + this->start;
      class Particle *pi = &(*this->parts)[ii];

      /* Skip non hydro particles */
      if (pi->type != type_hydro)
	continue;

      /* Check if need to do the interaction (for the ghost) */
      if ((type & interaction_ghost) && !pi->hydro.redo) {
	continue;
      }

      const float hig = pi->hydro.h * pi->hydro.h * kernel_gamma2;
      for(size_t j = 0; j < c->n_parts; j++) {
	size_t jj = j + c->start;
	/* Check if not trying to self interact */
	if (jj == ii) {
	  continue;
	}

	/* Get the second particle */
	class Particle *pj = &(*this->parts)[jj];

	/* Skip non hydro particles */
	if (pj->type != type_hydro)
	  continue;
	
	const float hjg = pj->hydro.h * pj->hydro.h * kernel_gamma2;

	/* Get distances */
	float r[3];
	float r2 = 0;
	for(int k = 0; k < 3; k++) {
	  r[k] = pi->pos[k] - pj->pos[k];

	  /* Apply periodic boundaries */
	  if (e->periodic) {
	    if (r[k] >= 0.5 * e->box_size) {
	      r[k] -= e->box_size;
	    }
	    else if (r[k] < -0.5 * e->box_size) {
	      r[k] += e->box_size;
	    }
	  }

	  r2 += r[k] * r[k];
	}

	/* Do the interaction */
	if (type & interaction_density) {
	  if (r2 > hig)
	    continue;
	  pi->density(pj, r2, r, e);
	}
	if (type & interaction_hydro_force) {
	  if (r2 > hjg)
	    continue;
	  pi->force(pj, r2, r, e);
	}
      }
    }
  }
}

/**
 * @brief Check if can do a cell - particle interaction
 *
 * @param c The #Cell to interact with
 * @param e The #Engine
 */
bool Cell::gravityCanInteractCells(class Cell *c, class Engine *e) {
  if (c == this)
    return false;

  /* Check if the angle is small enough */
  if (e->opening_angle < this->cellAngle(c))
    return false;

  return true;  
}


/**
 * @brief Check if gravity should recurse to children
 *
 * @param c The #Cell to interact with
 * @param e The #Engine
 */
bool Cell::gravityShouldRecurse(class Cell *c, class Engine *e) {
  if (c->is_leaf || this->is_leaf)
    return false;

  /* Check if the opening angle is small enough to do a cell interaction */
  if (this->cellAngle(c) < e->opening_angle)
    return false;

  return true;
}

/**
 * @brief update the particles
 *
 * @param type The type of update
 * @param e The #Engine
 */
void Cell::updateParticles(int type, class Engine *e) {
  bool with_hydro = e->policy & policy_hydro;

  /* Loop over all particles */
  for(size_t i = 0; i < this->n_parts; i++) {
    size_t ii = i + this->start;
    class Particle *pi = &(*this->parts)[ii];

    /* WARNING the order matter */
    if (type & update_acceleration) {
      pi->resetAcceleration(with_hydro);
    }

    if (type & update_end_force && pi->type == type_hydro) {
      pi->hydroEndForce();
    }

    if (type & update_cooling && pi->type == type_hydro) {
      pi->cooling(e->dt);
    }

    if (type & update_half_kick) {
      /* Do half a kick */
      pi->kick(0.5 * e->dt, with_hydro);
    }

    if (type & update_reset_predict && pi->type == type_hydro) {
      pi->hydroResetPredictedValues(e);
    }

    if (type & update_drift) {
      pi->drift(e->dt, e);
      if (pi->type == type_hydro) {
	pi->hydroInitPart();
      }
    }

    /* Do the sink particle formation */
    if (type & update_sink_formation && pi->type == type_hydro) {
      pi->sinkFormation(e->dt, e);
    }

  }
  
}

/**
 * @brief Compute the angle between two cells
 *
 * @param ci The #Cell to interact with
 */
float Cell::cellAngle(class Cell *ci) {
  /* Compute the distance */
  float d = 0;
  for(int i = 0; i < 3; i++) {
    float tmp = this->pos[i] - ci->pos[i];
    d += tmp * tmp;
  }

  d = sqrt(d);

  /* Compute the angle */
  return 2 * atan(0.5 * ci->cell_size / d);
  
}

/**
 * @brief Compute the cell time step
 *
 * @param e The #Engine
 */
float Cell::getTimeStep(class Engine *e) {
  /* Initialize the time step */
  float dt = FLT_MAX;

  /* Loop over all particles */
  for(size_t i = 0; i < this->n_parts; i++) {
    /* Get the particle */
    size_t ii = i + this->start;
    class Particle *pi = &(*this->parts)[ii];

    /* Compute the time step */
    float tmp = pi->getTimeStep(e);
    if (tmp < dt)
      dt = tmp;
  }

  return dt;
}


/**
 * @brief Do a ghost iteration
 *
 * @param e The #Engine
 * @param reset Are we computing the first ghost iteration?
 */
void Cell::doGhost(class Engine *e, bool reset) {
  /* Reset counter */
  this->ghost_todo = 0;
  this->h_max = 0;

  if (this->is_leaf) {
    /* Do the ghost on each particle */
    for(size_t i = 0; i < this->n_parts; i++) {
      /* Get the particle */
      size_t index = i + this->start;
      class Particle *p = &(*this->parts)[index];

      /* Check if it is a hydro part */
      if (p->type != type_hydro) {
	continue;
      }

      /* Check if the particle already converged */
      if (!p->hydro.redo && !reset) {
	continue;
      }

      /* Do the ghost */
      this->ghost_todo += p->ghost(e);

      /* Update h_max */
      if (p->hydro.h > this->h_max) {
	this->h_max = p->hydro.h;
      }
    }
  }

  /* Recurse */
  else {
    for(int i = 0; i < 8; i++) {
      /* Do the ghost on a children */
      class Cell *child = this->children[i];
      child->doGhost(e, reset);

      /* Update counter */
      this->ghost_todo += child->ghost_todo;

      /* Update h_max */
      if (child->h_max > this->h_max) {
	this->h_max = child->h_max;
      }

    }
  }
}
