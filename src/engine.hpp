#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <array>
#include <vector>

#include "cell.hpp"

#define engine_top_cell 2
#define engine_n_top_cells engine_top_cell * engine_top_cell * engine_top_cell


/**
 * Everything is in Myr, kpc, solMass
 */
class Engine {
public:
  /* The cells */
  std::array<class Cell, engine_n_top_cells> top_cells;

  /* The particles */
  std::vector<class Particle> parts;
  size_t n_parts;

  /* Simulation informations */
  float box_size;
  double dt;
  double dt_max;
  int step;
  double time;
  int policy;
  bool periodic;

  /* Gravity */
  float eps2;
  float opening_angle;

  /* hydro */
  int step_ghost;

  /* Stars */
  float r_acc;
  float c_star;

  /* Output */
  int snapshot_number;

  /* Constructor */
  Engine(std::vector<class Particle> parts, float box_size, float eps, double dt_max, bool periodic);

  /* Cell management */
  void tagParticles();
  void sortCells();
  void splitCells();
  void makeCells();
  void clean();
  void computeHmax();

  /* Operator */
  void print();
  void writeSnapshot();

  /* Gravity */
  void computeCenterMass();
  void disableGravity() {
    this->policy &= ~policy_grav;
  }

  /* Hydro */
  void disableHydro() {
    this->policy &= ~policy_hydro;
  }

  /* Stars */
  void disableStars() {
    this->policy &= ~policy_stars;
  }

  /* Cooling */
  void disableCooling() {
    this->policy &= ~policy_cooling;
  }
  


  /* Interactions */
  void doGravity();
  void doHydro(int type);
  void doGhost();

  /* Particle updates */
  void updateParticles(int type);
  float getTimeStep();
  void removeParticleOutOfBox();

  /* Main functions */
  void computeDensity();
  void doStep(float time);

  /* Debug */
private:
  void computeAcceleration();
  void computeMomentum();

};


#endif // ENGINE_HPP
