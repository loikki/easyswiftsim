#ifndef CELL_HPP
#define CELL_HPP

#include "particle.hpp"

#include <vector>

#define max_part_per_cell 200

class Engine;

/**
 * get the 3D (i, j, k) cell index from the 1D (i)
 */
inline void get_cell_index(int *i, int *j, int *k, int dim) {
  const int dim2 = dim * dim;
  int tmp = *i / (dim2);
  *j = (*i - tmp * dim2) / dim;
  *k = *i - (tmp * dim + *j) * dim;
  *i = tmp;
};


class Cell {
public:
  /* Variables for subcells */
  class Cell *children[8];
  bool is_leaf;

  /* Pointer to the global array */
  std::vector<class Particle> *parts;
  size_t n_parts;
  int start;

  /* Cell informations */
  float cell_size;
  float pos[3];
  int depth;

  /* Gravity information */
  float cm[3];
  float mass;

  /* Hydro information */
  float h_max;
  size_t ghost_todo;

  /* Constructor */
  Cell(float cell_size, float pos[3], std::vector<class Particle> *parts);
  Cell() {};


  /* test functions */
  bool inCell(class Particle *p);

  /* Gravity functions */
  float cellAngle(class Cell *ci);
  void computeCenterMass();
  bool gravityCanInteractCells(class Cell *c, class Engine *e);
  bool gravityShouldRecurse(class Cell *c, class Engine *e);

  /* hydro functions */
  void getHmax();
  bool hydroShouldRecurse(class Cell *c, class Engine *e);
  bool hydroCanInteract(class Cell *c, class Engine *e);
  void hydroInteractCells(class Cell *c, int type, class Engine *e);
  void doGhost(class Engine *e, bool reset);

  /* Cells management */
  void splitCell();
  void sort();
  void clean();

  /* Interaction functions */
  void gravityInteractCells(class Cell *c, class Engine *e);
  void gravityInteractCellLevel(class Cell *c, class Engine *e);

  /* Particle updates */
  void updateParticles(int type, class Engine *e);
  float getTimeStep(class Engine *e);

  /* Operator */
  void print();
};

#endif // CELL_HPP
