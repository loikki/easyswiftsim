#ifndef ENUM_HPP
#define ENUM_HPP

enum interaction_type : int {
  interaction_self = 1,
  interaction_grav = 2,
  interaction_density = 4,
  interaction_hydro_force = 8,
  interaction_ghost = 16,
  interaction_sink = 32,
};

enum update_type : int {
  update_acceleration = 1,
  update_half_kick = 2,
  update_drift = 4,
  update_ghost = 8,
  update_end_force = 16,
  update_reset_predict = 32,
  update_sink_formation = 64,
  update_cooling = 128,
};


enum policy : int {
  policy_hydro = 1,
  policy_grav = 2,
  policy_stars = 4,
  policy_cooling = 8,
};


#endif // ENUM_HPP
