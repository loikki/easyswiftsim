cmake_minimum_required(VERSION 3.13)

project(SINK)

#  -DDEBUG_INTERACTIONS_SPH
add_definitions(-Wall -fopenmp -DDEBUG_INTERACTIONS_SPH)

find_package(PythonInterp 3 REQUIRED)
find_package(PythonLibs 3 REQUIRED)

find_package(Boost REQUIRED COMPONENTS python36 system filesystem)

message("Boost library = ${Boost_LIBRARIES}" )
message("Boost python library = ${Boost_PYTHON_LIBRARY}" )

# Get library files
file(GLOB src src/*.cpp)
get_filename_component(full_path ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp ABSOLUTE)
list(REMOVE_ITEM src ${full_path})

add_library(sink_wrapper SHARED ${src})
target_link_libraries(sink_wrapper ${Boost_LIBRARIES} ${PYTHON_LIBRARIES} gomp Boost::filesystem)


set_target_properties(sink_wrapper PROPERTIES COMPILE_FLAGS "-I${PYTHON_INCLUDE_DIRS} -DWITH_PYTHON")


# Get executable files
file(GLOB src src/*.cpp)
get_filename_component(full_path ${CMAKE_CURRENT_SOURCE_DIR}/src/wrapper.cpp ABSOLUTE)
list(REMOVE_ITEM src ${full_path})

add_executable(sink ${src})
target_link_libraries(sink gomp -fsanitize=address -fno-omit-frame-pointer Boost::filesystem)
target_compile_options(sink PRIVATE -Werror -fsanitize=address -fno-omit-frame-pointer)


