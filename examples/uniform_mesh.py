#!/usr/bin/env python

import libsink_wrapper as sink
import numpy as np
import matplotlib.pyplot as plt
from h5py import File
from sedov_solution import sedov
from tools import tools

# Parameters
N = 16
m_part = 30.  # in solMass
rho = 100.  # in atom / cm3
T = 1e4  # in K

t_end = 10  # in Myr
dt_max = 1e-1
N_step = 10  # number of step (for the images)

# --------------------------

numPart = N**3
gamma = 5./3.      # Gas adiabatic index
fileName = "sedov.hdf5"
rho *= 24713211.  # convert into solMass / kpc3
mumh_kb = 1.158e8
u_init = T / ((gamma - 1.) * mumh_kb)

periodic = False
boxsize = (numPart * m_part / rho)**(1./3.)

glass = File("glassCube_{}.hdf5".format(N), "r")

# Read particle positions and h from the glass
p = glass["/PartType0/Coordinates"][:, :] * boxsize
p[p < 0] += boxsize
p[p >= boxsize] -= boxsize
h = glass["/PartType0/SmoothingLength"][:] * boxsize
p = p.astype(np.float32)
h = h.astype(np.float32) * 0.1

# Generate extra arrays
m = m_part * np.ones(numPart, dtype=np.float32)
u = np.zeros(numPart, dtype=np.float32)

u[:] = u_init


def plot(e):
    plotParticles(e)


def plotParticles(e):
    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)
    pos = tools.getPosition(e)
    dx = boxsize / N
    ind = np.abs(pos[:, 2] - 0.5 * boxsize) < 2 * dx
    vel = tools.getVelocity(e)
    u = tools.getEnergy(e)
    plt.scatter(pos[ind, 0], pos[ind, 1], c=u[ind])
    if (np.abs(vel).max() != 0):
        plt.quiver(pos[ind, 0], pos[ind, 1], vel[ind, 0], vel[ind, 1])

    plt.colorbar()


parts = sink.ParticleList()
pos = sink.float_vector()
vel = sink.float_vector()
for i in range(3):
    pos.append(0)
    vel.append(0)
eps = boxsize / (30. * N)

for i in range(N**3):
    for j in range(3):
        tmp = float(p[i, j])
        pos[j] = tmp
    m_i = float(m[i])
    h_i = float(h[i])
    u_i = float(u[i])
    parts.append(sink.Particle(pos, vel, m_i, h_i, u_i))

e = sink.Engine(parts, boxsize, eps, dt_max, periodic)

t = np.linspace(0, t_end, N_step)
for i in range(N_step):
    e.doStep(t[i])
    plot(e)
    plt.show()
