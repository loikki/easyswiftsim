#!/usr/bin/env python

import libsink_wrapper as sink
import numpy as np
import matplotlib.pyplot as plt
from h5py import File
from sedov_solution import sedov
from tools import tools

N = 16
boxsize = 1.
periodic = True

t_end = 5e-2  # in Myr
dt_max = 1e-2
N_step = 10  # number of step (for the images)

# Generates a swift IC file for the Sedov blast test in a periodic cubic box

# Parameters
gamma = 5./3.      # Gas adiabatic index
rho0 = 1.          # Background density
P0 = 1.e-6         # Background pressure
E0 = 1.             # Energy of the explosion
N_inject = 15      # Number of particles in which to inject energy
fileName = "sedov.hdf5"

# ---------------------------------------------------
glass = File("glassCube_{}.hdf5".format(N), "r")

# Read particle positions and h from the glass
p = glass["/PartType0/Coordinates"][:, :]
p[p < 0] += boxsize
p[p >= boxsize] -= boxsize
h = glass["/PartType0/SmoothingLength"][:]
p = p.astype(np.float32)
h = h.astype(np.float32) * 0.1

numPart = np.size(h)
vol = 1.

# Generate extra arrays
m = np.zeros(numPart, dtype=np.float32)
u = np.zeros(numPart, dtype=np.float32)
r = np.zeros(numPart)

r = np.sqrt((p[:, 0] - 0.5)**2 + (p[:, 1] - 0.5)**2 + (p[:, 2] - 0.5)**2)
m[:] = rho0 * vol / numPart
u[:] = P0 / (rho0 * (gamma - 1))

# Make the central particles detonate
index = np.argsort(r)
u[index[0:N_inject]] = E0 / (N_inject * m[0])


def plot(e):
    plotProfile(e)


def plotParticles(e):
    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)
    pos = tools.getPosition(e)
    dx = boxsize / N
    ind = np.abs(pos[:, 2] - 0.5 * boxsize) < 2 * dx
    vel = tools.getVelocity(e)
    u = tools.getEnergy(e)
    plt.scatter(pos[ind, 0], pos[ind, 1], c=u[ind])
    if (np.abs(vel).max() != 0):
        plt.quiver(pos[ind, 0], pos[ind, 1], vel[ind, 0], vel[ind, 1])

    plt.colorbar()


def plotProfile(e):
    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)
    pos = tools.getPosition(e)
    center_pos = 0.5 * np.array([boxsize]*3)
    r = np.sum((pos - center_pos)**2, axis=1)
    r = np.sqrt(r)

    u = tools.getEnergy(e)
    plt.plot(r, u, '.')
    # plt.ylim(-0.01 * u_blast, 1.1 * u_blast)
    tmp = sedov(e.time, E0, rho0, gamma)

    r = tmp[0]
    P = tmp[1]
    rho = tmp[2]
    u_s = P / (rho * (gamma - 1.))

    plt.plot(r, u_s, "--")
    e = E0 / (N_inject * m[0])
    plt.ylim(-0.01 * e, 1.1 * e)


parts = sink.ParticleList()
pos = sink.float_vector()
vel = sink.float_vector()
for i in range(3):
    pos.append(0)
    vel.append(0)
eps = boxsize / (30. * N)

for i in range(N**3):
    for j in range(3):
        tmp = float(p[i, j])
        pos[j] = tmp
    m_i = float(m[i])
    h_i = float(h[i])
    u_i = float(u[i])
    parts.append(sink.Particle(pos, vel, m_i, h_i, u_i))

e = sink.Engine(parts, boxsize, eps, dt_max, periodic)
e.disableGravity()
e.disableStars()
e.disableCooling()

t = np.linspace(0, t_end, N_step)
for i in range(N_step):
    e.doStep(t[i])
    e.writeSnapshot()
    plot(e)
    plt.show()
