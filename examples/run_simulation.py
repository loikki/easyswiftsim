#!/usr/bin/env python

import libsink_wrapper as sink
import numpy as np
import matplotlib.pyplot as plt

unit_rho = 4.04e-8  # Myr / kpc3 -> atom / cm3

mass = 100.
N_parts = 1000
t_end = 1.  # in Myr
dt = 0.01  # in Myr
dt_max = 1e6
u = 1  # in P / rho -> P kpc3 / solMass -> kpc2 / Myr2
pos = sink.float_vector()
vel = sink.float_vector()
for i in range(3):
    pos.append(0)
    vel.append(0)
box_size = 1.
eps = box_size / (30. * N_parts**(1./3.))

h = 3 * box_size / N_parts**(1./3.)
density = mass * N_parts / box_size**3
print("Density: {} [atom / cm3]".format(density * unit_rho))


def plotParticles(e):
    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)
    pos = getPosition(e)
    vel = getVelocity(e)
    plt.plot(pos[:, 0], pos[:, 1], ".")
    if (np.abs(vel).max() != 0):
        plt.quiver(pos[:, 0], pos[:, 1], vel[:, 0], vel[:, 1])


parts = sink.ParticleList()
for i in range(N_parts):
    pos[:] = np.random.rand(3)
    parts.append(sink.Particle(pos, vel, mass, h, u))

e = sink.Engine(parts, box_size, eps, dt_max)


def getPosition(e):
    pos = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        pos[i, :] = e.parts[i].pos

    return pos


def getVelocity(e):
    pos = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        pos[i, :] = e.parts[i].vel

    return pos


def getAcceleration(e):
    pos = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        pos[i, :] = e.parts[i].acc

    return pos


plotParticles(e)
plt.show()

N_step = int(t_end / dt)
t = np.linspace(0, t_end, N_step)
for i in range(N_step):
    e.doStep(t[i])
    plotParticles(e)
    plt.show()
