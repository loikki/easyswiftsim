#!/usr/bin/env python

import libsink_wrapper as sink
import numpy as np
import matplotlib.pyplot as plt
from tools import tools

print("You need to fix G to 1")

N = 16

t_end = 0.8  # in Myr
dt_max = 1e-3
N_step = 10  # number of step (for the images)
with_hydro = True
gamma = 5./3.
periodic = False
eps = 0.001

# Generates a swift IC file for the Sedov blast test in a periodic cubic box

# Parameters
M = 1.  # total mass of the sphere
R = 1.               # radius of the sphere
u0 = 0.05 / M        # initial thermal energy
numPart = int(N**3)

r = R * np.sqrt(np.random.random(numPart))
phi = 2. * np.pi * np.random.random(numPart)
cos_theta = 2. * np.random.random(numPart) - 1.

sin_theta = np.sqrt(1. - cos_theta**2)
cos_phi = np.cos(phi)
sin_phi = np.sin(phi)

p = np.zeros((numPart, 3))
p[:, 0] = r * sin_theta * cos_phi
p[:, 1] = r * sin_theta * sin_phi
p[:, 2] = r * cos_theta

# shift particles to put the sphere in the centre of the box
boxsize = 100. * R
p += 0.5 * np.array([boxsize]*3)

h = np.ones(numPart) * 2. * R / N

# Generate extra arrays
v = np.zeros((numPart, 3))
ids = np.linspace(1, numPart, numPart)
m = np.ones(numPart) * M / numPart
u = np.ones(numPart) * u0

ref = np.loadtxt("./examples/evrardCollapse3D_exact.txt")


def plot(e):
    # plotParticles(e)
    plotProfile(e)


def plotParticles(e):
    proj = 2
    if proj == 0:
        i1 = 1
        i2 = 2
    elif proj == 1:
        i1 = 0
        i2 = 2
    else:
        i1 = 0
        i2 = 1

    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)
    pos = tools.getPosition(e)
    dx = boxsize / N
    ind = np.abs(pos[:, proj] - 0.5 * boxsize) < 2 * dx
    if with_hydro:
        u = tools.getEnergy(e)
        plt.scatter(pos[ind, i1], pos[ind, i2], c=u[ind], s=10)
        plt.colorbar()
    else:
        plt.plot(pos[ind, i1], pos[ind, i2], ".")

    # vel = getVelocity(e)
    # if (np.abs(vel).max() != 0):
    #     plt.quiver(pos[ind, 0], pos[ind, 1], vel[ind, 0], vel[ind, 1])

    # plot solution
    plt.axis("equal")


def plotEnergy(e, r):
    plt.ylabel("Energy")

    u = tools.getEnergy(e)
    plt.loglog(r, u, '.r', ms=0.2)

    plt.loglog(ref[:, 0], ref[:, 3] / ref[:, 1] / (gamma - 1.), "k--",
               alpha=0.8, lw=1.2)

    plt.xlim(1.e-3, 2.)
    plt.ylim(1.e-2, 2.)


def plotVelocity(e, r):
    plt.ylabel("Velocity")
    v = tools.getVelocity(e)
    v = -np.sum(v**2, axis=1)

    plt.semilogx(r, v, ".r", ms=0.2)
    plt.semilogx(ref[:, 0], ref[:, 2], "k--", alpha=0.8, lw=1.2)
    plt.xlim(1.e-3, 2.)
    plt.ylim(-1.7, 0.1)


def plotDensity(e, r):
    plt.ylabel("Density")
    rho = tools.getDensity(e)

    plt.loglog(r, rho, '.r', ms=0.2)
    plt.loglog(ref[:, 0], ref[:, 1], "k--", alpha=0.8, lw=1.2)
    plt.xlim(1.e-3, 2.)
    plt.ylim(1.e-2, 1.e4)


def plotPressure(e, r):
    plt.ylabel("Pressure")
    P = tools.getPressure(e)

    plt.loglog(r, P, ".r", ms=0.2)
    plt.loglog(ref[:, 0], ref[:, 3], "k--", alpha=0.8, lw=1.2)
    plt.xlim(1.e-3, 2.)
    plt.ylim(1.e-4, 1.e3)


def plotProfile(e):
    plt.figure()
    plt.title("Time: %.2f Myr" % e.time)

    pos = tools.getPosition(e)
    center_pos = 0.5 * np.array([boxsize]*3)
    r = np.sum((pos - center_pos)**2, axis=1)
    r = np.sqrt(r)

    plt.subplot(221)
    plotVelocity(e, r)

    plt.subplot(222)
    plotDensity(e, r)

    plt.subplot(223)
    plotPressure(e, r)

    plt.subplot(224)
    plotEnergy(e, r)


parts = sink.ParticleList()
pos = sink.float_vector()
vel = sink.float_vector()
for i in range(3):
    pos.append(0)
    vel.append(0)

for i in range(N**3):
    for j in range(3):
        tmp = float(p[i, j])
        pos[j] = tmp
    m_i = float(m[i])
    h_i = float(h[i])
    u_i = float(u[i])
    parts.append(sink.Particle(pos, vel, m_i, h_i, u_i))

e = sink.Engine(parts, boxsize, eps, dt_max, periodic)
e.disableStars()
e.disableCooling()
if not with_hydro:
    e.disableHydro()


t = np.linspace(0, t_end, N_step)
for i in range(N_step):
    e.doStep(t[i])
    plot(e)
    plt.show()
