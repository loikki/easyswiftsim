import numpy as np


def getPosition(e):
    pos = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        pos[i, :] = e.parts[i].pos

    return pos


def getEnergy(e):
    u = np.zeros((e.n_parts))
    for i in range(e.n_parts):
        u[i] = e.parts[i].getEnergy()

    return u


def getVelocity(e):
    pos = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        pos[i, :] = e.parts[i].vel

    return pos


def getAcceleration(e):
    acc = np.zeros((e.n_parts, 3))
    for i in range(e.n_parts):
        acc[i, :] = e.parts[i].acc

    return acc


def getPressure(e):
    p = np.zeros(e.n_parts)
    for i in range(e.n_parts):
        p[i] = e.parts[i].getPressure()

    return p


def getDensity(e):
    rho = np.zeros(e.n_parts)
    for i in range(e.n_parts):
        rho[i] = e.parts[i].getDensity()

    return rho
