import numpy as np

hydro_dtype = np.dtype([
    ("pos", np.float32, 3),
    ("vel", np.float32, 3),
    ("acc", np.float32, 3),
    ("mass", np.float32, 1),
    ("id", np.int32, 1),
    ("h", np.float32, 1),
    ("rho", np.float32, 1),
    ("u", np.float32, 1),
])


def readHydro(filename):
    data = np.fromfile(filename, dtype=hydro_dtype)

    return data
